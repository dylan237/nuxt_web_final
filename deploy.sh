date=`date +%Y%m%d%H%M%S`

if [ "$1" != "" ]; then
  echo " start ${date}!"
  if [ "$1" = "sit" ]; then
    # 佈署 sit 
    if [ "$2" != "" ]; then
      # 有第二個參數的話自動 commit 
      git add . 
      git commit -m $2"-${date}"
    fi
    git checkout develop
    git pull origin develop
    git pull origin sit
    git checkout sit
    git merge develop -m "${date}"
    git push origin sit
    git checkout develop
    echo "▇▇▇▇  sit 佈署中，請去 gitlab Pipelines 查看最新情況 ▇▇▇▇"
  fi

  if [ "$1" = "prod" ]; then
    # 佈署 prod 
    read -p "你確定要發射飛彈 (y9527/n)?" CONT
    if [ "$CONT" = "y9527" ]; then
      git checkout develop
      git pull origin develop
      git pull origin master
      git checkout master
      git merge develop -m "${date}"
      git push origin master
      git checkout develop
      echo "▇▇▇▇ 提醒：佈署作業已進入啟動程序，若沒意外飛彈將於不久之後發射，請去 gitlab Pipelines 查看最新情況，祝好運。▇▇▇▇"
    else
      echo "▇▇▇▇ 您取消了佈署作業 ▇▇▇▇"
      exit 0
    fi
  fi
  exit 0
else
  echo "▇▇▇▇  請輸入完整的佈署指令 ▇▇▇▇▇"
fi
