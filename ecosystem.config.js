module.exports = {
	apps : [{
		name: 'wayne1894_nuxt_prod',
		script: './node_modules/nuxt-start/bin/nuxt-start.js',
		instances: 'max', //負載平衡模式下的 cpu 數量
		exec_mode : "cluster", //cpu 負載平衡模式
		max_memory_restart: '1G', //緩存了多少記憶體重新整理
		port: 3001, //指定伺服器上的 port
	},
    {
		name: 'wayne1894_nuxt_sit',
		script: './node_modules/nuxt-start/bin/nuxt-start.js',
		instances: 'max', //負載平衡模式下的 cpu 數量
		exec_mode : "cluster", //cpu 負載平衡模式
		max_memory_restart: '1G', //緩存了多少記憶體重新整理
		port: 3002, //指定伺服器上的 port
	}],
    deploy: {
      "prod": {
        "user": "wayne1894_teach", //linux 登入帳號 帳號@ip
        "host": ["107.167.189.45"], //你的伺服器 ip
        "ref": "origin/master", //分支
        "repo": "git@gitlab.com:wayne1894.teach/wayne1894_nuxt.git", //ssh 的 git
        "path": "/home/wayne1894_teach/wayne1894_nuxt", //伺服器上的路徑
        "post-deploy": "npm install && npm run build && pm2 reload ecosystem.config.js --env prod --only wayne1894_nuxt_prod", //佈署指令
        "env"  : {
          "NODE_ENV": "prod"
        }
      },
      "sit": {
        "user": "wayne1894_teach", //linux 登入帳號 帳號@ip
        "host": ["107.167.189.45"], //你的伺服器 ip
        "ref": "origin/sit", //分支
        "repo": "git@gitlab.com:wayne1894.teach/wayne1894_nuxt.git", //ssh 的 git
        "path": "/home/wayne1894_teach/wayne1894_nuxt_sit", //伺服器上的路徑
        "post-deploy": "npm install && npm run build_SIT && pm2 reload ecosystem.config.js --env sit --only wayne1894_nuxt_sit", //佈署指令
        "env"  : {
          "NODE_ENV": "sit"
        }
      }
    }
};

