import {sleep,deepCopy} from "./assets/js/tool.js"
import router from "./router.js"
import webpack from 'webpack'

console.log(process.env.NODE_ENV,"nuxt.config.js")

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: "wayne1894 教學網",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "網站前後端、網頁設計、程式語言教學網站" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script:
      [
        { src: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js" }
      ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#40659b', height:"5px", duration:5000 },
  transition: {
      name: 'layout',
      mode: 'out-in'
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/datepicker.js', mode: 'client' },
    { src: '~/plugins/gsap.js', mode: 'client' },
    { src: '~/plugins/qs.js'},
    { src: '~/plugins/axios.js'},
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    ['@nuxtjs/dotenv'], //這是給預設 .env 使用
    ['@nuxtjs/dotenv', { filename: '.env.' + process.env.NODE_ENV }] //這是給預設 .env 使用
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt'
  ],
//  proxy: [
//    'http://localhost:3034/api'
//  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    plugins: [
      new webpack.ProvidePlugin({
        _M: "~/constants.js",
      })
    ],
    extractCSS: true,
    extend (config, ctx) {
      //console.log(config)
    }
  },
  router: {
    mode: 'history', //這段可以不用寫
    extendRoutes (routes, resolve) {
      //return router
    }
  },
  serverMiddleware: [
    { path: '/auth', handler: '~/server/auth.js' },
  ],
//  env:{
//    firebaseApiKey: "AIzaSyCPy4LjbKoSHLFtpF_htjRInT4ARuh2rvk"
//  },
}
